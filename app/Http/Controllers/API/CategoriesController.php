<?php

namespace App\Http\Controllers\API;

use App\Category;
use App\Http\Resources\CategoryResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return CategoryResource::collection(Category::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
          'name' => 'required|min:3|max:191|unique:categories'
        ]);

        if ($validator->fails()) 
        {
            return response()->json([
                'status' => false,
                'errors'=> $validator->errors()
            ], 200);            
        }

        $category = Category::create([
            'name' => request('name')
        ]);

        return response()->json([
            'status' => true,
            'data' => new CategoryResource($category)
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::find($id);

        if (empty($category)) {
            return response()->json([
                'status' => false,
                "message" => "not found"
            ], 200);
        }

        return new CategoryResource($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::find($id);

        if (empty($category)) {
            return response()->json([
                'status' => false,
                "message" => "not found"
            ], 200);
        }

        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                'min:3',
                'max:191',
                Rule::unique('categories')->ignore($id)
           ]
        ]);

        if ($validator->fails()) 
        {
            return response()->json([
                'status' => false,
                'errors'=> $validator->errors()
            ], 200);            
        }

        $category->update([
            'name' => request('name')
        ]);

        return response()->json([
            'status' => true,
            'data' => new CategoryResource($category)
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);

        if (empty($category)) {
            return response()->json([
                'status' => false,
                "message" => "not found"
            ], 200);
        }

        $category->news()->sync([]);

        $category->delete();

        return response()->json([
            'status' => true,
            'create' => [
                'href' => route('categories.store'),
                'method' => 'POST',
                'params' => 'name'
            ]
        ]);
    }
}
