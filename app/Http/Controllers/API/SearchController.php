<?php

namespace App\Http\Controllers\API;

use App\News;
use App\Http\Resources\NewsResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{
    public function filter(Request $request)
    {
    	$q = News::query();

    	if ($request->has('title')) {
    		$q->where('title', 'like', "%{$request->title}%");
    	}

    	if ($request->has('date')) {
			$q->whereDate('created_at', '=', date('Y-m-d', strtotime($request->date)));
    	}

    	return NewsResource::collection($q->paginate(10)->sortByDesc('title')->sortByDesc('created_at'));
    }
}
