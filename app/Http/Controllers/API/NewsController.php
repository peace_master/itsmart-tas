<?php

namespace App\Http\Controllers\API;

use App\News;
use App\Http\Resources\NewsResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return NewsResource::collection(News::paginate(10)->sortByDesc('title')->sortByDesc('created_at'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
          'title' => 'required|min:3|max:191|unique:news',
          'short_description' => 'required',
          'text' => 'required'
        ]);

        if ($validator->fails()) 
        {
            return response()->json([
                'status' => false,
                'errors'=> $validator->errors()
            ], 200);            
        }

        $news = News::create([
            'title' => request('title'),
            'short_description' => request('short_description'),
            'text' => request('text')
        ]);

        $news->categories()->sync(request('category_id'));

        return response()->json([
            'status' => true,
            'data' => new NewsResource($news)
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $news = News::find($id);

        if (empty($news)) {
            return response()->json([
                'status' => false,
                "message" => "not found"
            ], 200);
        }

        return new NewsResource($news);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $news = News::find($id);

        if (empty($news)) {
            return response()->json([
                'status' => false,
                "message" => "not found"
            ], 200);
        }

        $validator = Validator::make($request->all(), [
            'title' => [
                'required',
                'min:3',
                'max:191',
                Rule::unique('news')->ignore($id)
           ],
           'short_description' => 'required',
           'text' => 'required'
        ]);

        if ($validator->fails()) 
        {
            return response()->json([
                'status' => false,
                'errors'=> $validator->errors()
            ], 200);            
        }

        $news->update([
            'title' => request('title'),
            'short_description' => request('short_description'),
            'text' => request('text')
        ]);

        return response()->json([
            'status' => true,
            'data' => new NewsResource($news)
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news = News::find($id);

        if (empty($news)) {
            return response()->json([
                'status' => false,
                "message" => "not found"
            ], 200);
        }

        $news->categories()->sync([]);

        $news->delete();

        return response()->json([
            'status' => true,
            'create' => [
                'href' => route('news.store'),
                'method' => 'POST',
                'params' => 'title, short_description, text'
            ]
        ]);
    }
}
