<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'view_category' => [
                'href' => route('categories.show', $this->id),
                'method' => 'GET'
            ]
        ];
    }

    public function with($request)
    {
        return ['status' => true];
    }
}
