<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NewsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'short_description' => $this->short_description,
            'text' => $this->text,
            'view_news' => [
                'href' => route('news.show', $this->id),
                'method' => 'GET'
            ]
        ];
    }

    public function with($request)
    {
        return ['status' => 'true'];
    }
}
