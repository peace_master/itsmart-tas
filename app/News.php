<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $fillable = [
    	'title', 'short_description', 'text'
    ];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($news) {
            $news->categories->each->delete();
        });
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }
}
